#!/bin/bash

# Main packages
sudo apt-get install -y apache2 php mariadb-server libapache2-mod-php

# Dependencies
sudo apt-get install -y php-xml php-json php-xdebug mailutils

# configuring the Apache
echo '
<VirtualHost *:80>
       
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

' > /etc/apache2/sites-enabled/000-default.conf; 

echo 'Apache config created\n'

echo 'Reloading Apache'
sudo systemctl reload apache2
echo 'Apache reloaded'